<?php
	session_start();
	
	include("phpscripts/phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
	
	$event_id = $_GET['event_id'];
	
	$link = openDatabase();
	
	//query for this particular event information
	$statement = "SELECT * FROM events WHERE event_id=" . $event_id;

	$event_info = queryWithAssocResult($statement); //assoc array of all data
	$event_info = $event_info[0];
	
	$link = openDatabase();
		
	//find each shift that the member is a part of
	$statement = "SELECT shift_id, comment FROM sign_ups WHERE event_id=" . $event_info['event_id'] . " AND user_id=" . $_SESSION['user_id'];
	$query = $link->prepare($statement);
	
	if ($query)
	{	
		$query->bind_result($shift_id, $comment);
		$query->execute();
		
		$i = 0;
		while ($query->fetch())
		{
			$shift_ids[] = $shift_id;
			$comments[] = $comment;
		}
		
		$query->close();
	}
	else
		echo "There was an error with your query: 2";
		
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div class="clear"></div>
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner" class="blue">
				<?php
				
					if (checkIfMember())
					{
						if (isset($_SESSION['sign_up_errors']))
						{
							echo "<div class='errors'>";
							echo $_SESSION['sign_up_errors'];
							echo "</div>";
							unset($_SESSION['sign_up_errors']);
						}
				?>
					<h1 align="center">Event Sign Up</h1>
				
					<!-- all information for the selected event -->
					<div id="event_info">
						<table align="center" width="100%">
							<tr style="vertical-align:text-top;">
								<td width="15%"><b>Event Name:</b><br /></td>
								<td><?php echo $event_info['event_name']; ?> <br /></td>
							</tr>
							<tr style="vertical-align:text-top;">
								<td width="15%"><b>Address:</b><br /></td>
								<td><?php echo $event_info['address']; ?> <br /></td>
							</tr>
							<tr style="vertical-align:text-top;">
								<td width="15%"><b>Starting Date:</b><br /></td>
								<td><?php echo dateToString($event_info['start_date']); ?> <br /></td>
							</tr>
							<tr style="vertical-align:text-top;">
								<td width="15%"><b>Additional Info:</b><br /></td>
								<td><?php echo $event_info['comments']; ?></td>
							</tr>
						</table>
						
						<?php
							if (!empty($event_info['map_link']))
							{
						?>
									<div style="width:105px; margin:0 auto;">
										<a class="button_a" style="text-align:center; width:100px;" href="<?php echo $event_info['map_link']; ?>" target="_blank">Event Location</a>
									</div>
						<?php
							}
						?>
						
						<table align="center" width="85%">
				
						
						<?php
							$statement = "SELECT * FROM event_files WHERE event_id=" . $event_info['event_id'] . " ORDER BY title";
							
							$rows = queryWithAssocResult($statement);
								
							if (!empty($rows))
							{
						?>
								<hr />
								<tr>
									<td><h3>Title<h3></td>
									<td><h3>Comment<h3></td>
									<td></td>
								</tr>
						<?php
							}
								
							foreach ($rows as $row)
							{
							?>
							<tr>
								<?php
									if (checkIfAdmin())
									{
										echo '<td width="5%"><form method="post" action="phpscripts/remove_event_file.php">
											<input type="submit" value="X" />
											<input type="hidden" name="file_id" value="' . $row['file_id'] . '" />
										</form></td>';
									}
								?>
								<td width="35%"> <?php echo $row['title']; ?> </td>
								<td width="45%"> <?php echo $row['comment']; ?> </td>
								<td width="10%">
								<?php
									if (file_exists("phpscripts/files/" . $row['filename']))
									{
								?>
											<a class="button_a" href="<?php echo "phpscripts/download_file.php?filename=" . $row['filename']; ?>">Download</a>
								<?php
									}
								?>
								</td>
							</tr>
						<?php
							}
						?>
								
						</table>
					</div>
					
					
					
					<h2 align="center">Shifts<h2>
						
					<?php
						
						//query for all shifts with this event id
						
						$statement = "SELECT * FROM shifts WHERE event_id=" . $event_id . " ORDER BY date";
						
							
							$previous_date = 0;
							
							$shift_infos = queryWithAssocResult($statement);
							
							$i = 0;
							//for each shift
							foreach ($shift_infos as $shift_info) //assoc array of all shift data, per shift
							{
								$same_day = false;
								
								if ($shift_info['date'] == $previous_date)
									$same_day = true;
								
								if (!$same_day)
								{
									if ($previous_date != 0) //will not run on first run-through
									{
									?>
										</div> <!-- This is the looped end tag for the "new_day" div -->
									<?php
									}
									?>
									<div class="new_day gradient1">
									<?php echo dateToString($shift_info['date']); ?>
								<?php
								}
								
								$previous_date = $shift_info['date'];
								
								$already_signed_up = false;
								//if ($shift_info['shift_id'] is in $shift_ids )
									//if the shift is in the array, put a check-mark where the sign_up button would be
								if (isset($shift_ids))
								{
									if (in_array($shift_info['shift_id'], $shift_ids))
									{
										$comment_index = array_search($shift_info['shift_id'], $shift_ids);
										$already_signed_up = true;
									}
								}
								
							?>
								
								<div class="shift_info">
									<table align="center" width="100%">
										<!-- table of shift information with a form for sign_up and comments -->
										<tr>
											<td>
												<table>
													<tr>
														<td> <b>Shift Start:</b><br /><?php echo date("g:i a", strtotime($shift_info['start_time'])); ?> </td>
														<td> <b>Shift End:</b><br /><?php echo date("g:i a", strtotime($shift_info['end_time'])); ?> </td>
													</tr>
													<tr>
														<td colspan="2">
															<form method="post" action="phpscripts/sign_up.php">
																<input type="hidden" name="event_id" value="<?php echo $event_id; ?>" />
																<input type="hidden" name="shift_id" value="<?php echo $shift_info['shift_id']; ?>" />
																
																<?php
																if (checkIfAdmin())
																{
																?>
																	<select name="selected_user_id">
																	<?php
																		$select_statement = "SELECT user_id, first, last FROM members WHERE status='Active'";
																	$rows = queryWithAssocResult($select_statement);
																			
																	echo '<option selected="true" value="0">[Select A Member]</option>';
																	foreach ($rows as $row)
																	{
																		echo '<option value="' . $row['user_id'] . '">' . $row['first'] . ' ' . $row['last'] . '</option>';
																	}
																	?>
																	</select>
																<?php
																}
																
																if ($shift_info['current_amount'] < $shift_info['required_amount'])
																	$statement = "Need " . ($shift_info['required_amount'] - $shift_info['current_amount']) . " more.";
																else if ($shift_info['current_amount'] > $shift_info['required_amount'])
																	$statement = "Have " . ($shift_info['current_amount'] - $shift_info['required_amount']) . " extra.";
																else
																	$statement = "Accepting Extras.";
																	
																if ($already_signed_up)
																{
																?>
																	<textarea type="text" name="user_comment" cols="20" readonly="readonly"><?php echo $comments[$comment_index]; ?></textarea>
																<?php
																	echo "Requires " . $shift_info['required_amount'] . " people.<br />";
																?>
																	<!-- TODO: get rid of font, it's depricated -->
																	<font color="#000000">
																		<?php echo $statement; ?>
																	</font>
																	<br />
																	<font color="#007700">
																		Already Signed Up
																	</font>
																	<br />
																	<?php
																	if (checkIfAdmin())
																	{
																	?>
																		<input type="submit" value="Sign Up" />
																<?php
																	}
																	
																}
																else
																{
																?>
																	<input type="text" name="user_comment" value="Enter a comment." maxlength="100" size="20" onclick="this.value=''; return true;" />
																<?php
																	echo "Requires " . $shift_info['required_amount'] . " people.<br />";
																?>
																	<br />
																	<font color="#0000FF">
																		<b>
																			<?php echo $statement; ?>
																		</b>
																	</font>
																	<br />
																	<input type="submit" value="Sign Up" />
																<?php
																}
																?>
															</form>
														</td>
													</tr>
												</table>
											</td>
											
											<td width="50%" style="border-left-style:solid;
																				border-color:#555555;
																				border-width:1px;
																				padding-left:5px;
																				vertical-align:text-top;"> <b>Required:</b> <br />
												<?php
													//find all names of required members
													$statement = "SELECT members.user_id, members.first, members.last, sign_ups.comment FROM members, sign_ups WHERE members.user_id=sign_ups.user_id AND sign_ups.required=1 AND sign_ups.shift_id=" . $shift_info['shift_id'];
													$query_names = $link->prepare($statement);
													if ($query_names)
													{
														$query_names->bind_result($user_id, $first, $last, $comment);
														$query_names->execute();
														while ($query_names->fetch())
														{
															echo $first . " " . $last;
															if (checkIfAdmin() or ($user_id == $_SESSION['user_id']))
															{
																if (!empty($comment))
																{
															?>
																<select style="width:100px;"><option> <?php echo $comment; ?> </option></select>
															<?php
																}
																
																if (checkIfAdmin())
																{
															?>
																	<form method="post" action="phpscripts/remove_sign_up.php" style="margin: 0px; padding: 0px; display:inline;">
																		<input type="hidden" name="selected_shift_id" value="<?php echo $shift_info['shift_id']; ?>" />
																		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
																		<input type="hidden" name="required" value="1" />
																		<input type="submit" value="x" />
																	</form>
															<?php
																}
															?>
																<form method="post" action="phpscripts/required_to_extra.php" style="margin: 0px; padding: 0px; display:inline;">
																	<input type="hidden" name="selected_shift_id" value="<?php echo $shift_info['shift_id']; ?>" />
																	<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
																	<input type="hidden" name="required" value="1" />
																	<input type="submit" value=">>" />
																</form>
															<?php
															}
															
															echo "<br />";
														}
														$query_names->close();
													}
													echo $link->error;
												?>
											
											</td>
											<td width="50%" style="border-left-style:solid;
																				border-color:#555555;
																				border-width:1px;
																				padding-left:5px;
																				vertical-align:text-top;"> <b>Extra:</b> <br />
												<?php
												
													//find all names of extra members
													$statement = "SELECT members.user_id, members.first, members.last, sign_ups.comment FROM members, sign_ups WHERE members.user_id=sign_ups.user_id AND sign_ups.required=0 AND sign_ups.shift_id=" . $shift_info['shift_id'];
													$query_names = $link->prepare($statement);
													if ($query_names)
													{
														$query_names->bind_result($user_id, $first, $last, $comment);
														$query_names->execute();
														while ($query_names->fetch())
														{
															echo $first . " " . $last . " ";
															if (checkIfAdmin() or ($user_id == $_SESSION['user_id']))
															{
																if (checkIfAdmin())
																{
															?>
																	<form method="post" action="phpscripts/remove_sign_up.php" style="margin: 0px; padding: 0px; display:inline;">
																		<input type="hidden" name="selected_shift_id" value="<?php echo $shift_info['shift_id']; ?>" />
																		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
																		<input type="hidden" name="required" value="0" />
																		<input type="submit" value="x" />
																	</form>
														<?php
																}
														?>
																<form method="post" action="phpscripts/extra_to_required.php" style="margin: 0px; padding: 0px; display:inline;">
																	<input type="hidden" name="selected_shift_id" value="<?php echo $shift_info['shift_id']; ?>" />
																	<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
																	<input type="hidden" name="required" value="0" />
																	<input type="submit" value="<<" />
																</form>
																
														<?php
																if (!empty($comment))
																{
																?>
																	<select style="width:100px;"><option> <?php echo $comment; ?> </option></select>
														<?php
																}
															}
															
															
															echo "<br />";
														}
														$query_names->close();
													}
													echo $link->error;
												?>
												
												<!-- Add an option to move an extra into a required? Or do it automatically? -->
											</td>
										</tr>
										<tr>
											<td style="text-align:center;" colspan="2">
								
											</td>
										</tr>
									</table>
								</div>
							
							<?php
								$i++;
							} //end of the while loop
							
							echo "</div>"; //end div for new_day div at the end of the loop
						
						//include all available shifts with the sign_up button
					} //end of member check
					?>
				
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
				
				<div class="clear"></div>
			</div>
		</div> <!-- END OF MAIN DIV -->
		
	</body>
</html>

<?php
	if ($link)
		$link->close();
?>