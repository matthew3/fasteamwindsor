<?php
	session_start();
	
	include("phpscripts/phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div class="clear"></div>
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner">
				<?php
					if (checkIfMember())
					{
				?>
						<h1 style="text-align:center;">
							My Events
						</h1>
						
						<?php
							$link = openDatabase();
							
							for ($i = 1; $i >= 0; $i--)
							{
								$event_ids = array();
								$shift_ids = array();
							
								if ($i == 0)
									echo '<h2 align="center"> Extra (Not Required) Shifts </h2>';
								else
									echo '<h2 align="center"> Required Shifts </h2>';
								
								$statement = "SELECT shift_id, event_id FROM sign_ups WHERE required=" . $i . " AND user_id=" . $_SESSION['user_id'] . " ORDER BY event_id DESC, shift_id DESC";
								$query = $link->prepare($statement);
								
								if ($query)
								{
									$query->bind_result($shift_id, $event_id);
									$query->execute();
									while($query->fetch())
									{
										$shift_ids[] = $shift_id;
										$event_ids[] = $event_id;
									}
									$query->close();
									
									if (count($event_ids) > 0)
									{
										//query for all events in array event_ids, sort by (date)
										$statement = "SELECT event_id, event_name, start_date, organization FROM events WHERE event_id IN (" . implode(',', array_map('intval', $event_ids)) . ") ORDER BY start_date ASC";
										
										$event_results = array();
										$rows = queryWithAssocResult($statement);
										
										foreach ($rows as $row)
										{
											$event_results[] = $row;
										}
										
										/*
											value in php_array
											$sql = 'SELECT * 
												  FROM `table` 
												 WHERE `id` IN (' . implode(',', array_map('intval', $array)) . ')';
										*/
										
										//previous id != event_id indicates a new event
										$previous_id = 0;
										foreach ($event_results as $event_info)
										{
											//new event, start new div
											if ($previous_id != $event_info['event_id'])
											{
												if ($previous_id != 0)
												{
													echo "</div>";// <!-- the ending div for the previous event -->
												}
												?>
													<div class="new_day">
												<?php echo "<b style='font-size:22;'>" . $event_info['event_name'] . ": " . dateToString($event_info['start_date']) . "</b>
																	<br />" . $event_info['organization'];
																	
												//query for all shifts that are in array shift_ids where event_id is the current event, sort by (date, time)
												$statement = "SELECT * FROM shifts WHERE event_id=" . $event_info['event_id'] . " AND shift_id IN (" . implode(',', array_map('intval', $shift_ids)) . ") ORDER BY date ASC, start_time ASC";
												$rows = queryWithAssocResult($statement);
												
												foreach ($rows as $row)
												{
													//CAN: add suffixes to times and convert from military
												?>
														<div class="shift_info">
															<table>
																<tr>
																	<td colspan="2">
																		<b>
																			<?php echo dateToString($row['date']); ?>
																		</b>
																	</td>
																</tr>
																<tr>
																	<td>
																		<b>
																			Start Time: <br />
																			End Time:
																		</b>
																	</td>
																	<td>
																		<?php echo $row['start_time']; ?> <br />
																		<?php echo $row['end_time']; ?>
																	</td>
																</tr>
															</table>
														</div>
												<?php
												}
												
											}
											
											$previous_id = $event_info['event_id'];
											
										} //end of foreach loop going through event_ids
										
									} //end of if has events
									else
										echo '<div class="shift_info">No shifts to be found.<br />Starting signing up <a href="all_events_page.php">here</a>.</div>';
									
									if (count($event_ids) > 0)
										echo "</div>"; //end of "new_day" div
									
								} //end of query
								else
									echo $link->error;
							}
							
							$link->close();
						} //end of check if member
					?>
					
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
				
				<div class="clear"></div>
			</div>
		</div> <!-- END OF MAIN DIV -->
		
	</body>
</html>