<?php
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner">
					<h3 align="center">Sorry</h3>
					<p>
						The page you have requested does not exist or you do not have permission to view it.
						<br />
						Click <a href="/index.php">here</a> to go to the main page.
					</p>
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
			</div>
		</div> <!-- END OF MAIN DIV -->
	</body>
</html>