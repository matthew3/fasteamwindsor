<?php
	function openDatabase()
	{
		//open database
		//database error checking
		//$link = new MySQLi("localhost", "root", "Em15681568", "fasteam_db");
		$link = new MySQLi("localhost", "matthew3_eric", "Em15681568", "matthew3_fasteam_db");
		if ($link->connect_errno)
		{
			echo "There was a problem connecting to the database: " . $link->connect_errno . ": " . $link->connect_error;
		}
		
		return $link;
	}
	
	function queryWithAssocResult($statement)
	{
		$retArray = array();
	
		$conn = mysql_connect("localhost", "matthew3_eric", "Em15681568");
		
		if (mysql_select_db("matthew3_fasteam_db"))
		{
			$result = mysql_query($statement);
			if ($result)
			{
				while ($row = mysql_fetch_assoc($result))
				{
					$retArray[] = $row;
				}
			}
			else
				echo "QueryWithAssoc: There was an error with the query";
		}
		else
			echo "QueryWithAssoc: Error connecting to DB.";
		
		mysql_close($conn);
		
		return $retArray;
	}
	
	function checkIfAdmin()
	{
		if (isset($_SESSION['user_id']))
		{
			if ($_SESSION['position'] === 'Coordinator' or $_SESSION['position'] === 'Admin' or $_SESSION['Position'] == 'Assistant_Coordinator')
			{
				return true;
			}
		}
		return false;
	}
	
	function checkIfMember()
	{
		if (isset($_SESSION['user_id']))
		{
			if (isset($_SESSION['position']))
			{
				return true;
			}
		}
		echo "Authentication Error: I'm sorry, you are not authorized to view this page, you must log in. ";
		return false;
	}
	
	function dateToString($date) //date is in format yyyy-mm-dd
	{
		$date_time = date_create($date);
		return date_format($date_time, "l, F jS Y");
	}
	
	function insertIntoLog($name, $desc, $date)
	{
		$link = openDatabase();
	
		if (!$link)
		{
			echo "insertIntoLog: Could not open database.";
			return 0;
		}
	
		$statement = "INSERT INTO member_log VALUES (DEFAULT(log_id),'" . $name . "','" . $desc . "','" . $date . "')";
		
		$query = $link->prepare($statement);
		if ($query)
		{
			$ok = $query->execute();
			if (!$ok)
				echo "insertIntoLog: Query failed.";

			$query->close();
		}
		else
			echo "Error with the query: 1";
			
		$link->close();
	}
	
	function outputGoogleMapFromAddress($address) //outputs a 400x400 frame with a google map on it
	{
		// First, setup the variables you will use on your <iframe> code
		// Your Iframe will need a Width and Height set
		// as well as the address you plan to Iframe
		// Don't forget to get a Google Maps API key

		$latitude = '';
		$longitude = '';
		$iframe_width = '400px';
		$iframe_height = '400px';
		$address = urlencode($address);
		$key = "YOUR GOOGLE MAPS API KEY";
		$url = "http://maps.google.com/maps/geo?q=".$address."&output=json&key=".$key;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER,0);
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
		// Comment out the line below if you receive an error on certain hosts that have security restrictions
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$data = curl_exec($ch);
		curl_close($ch);

		$geo_json = json_decode($data, true);

		// Uncomment the line below to see the full output from the API request
		// var_dump($geo_json);

		// If the Json request was successful (status 200) proceed
		if ($geo_json['Status']['code'] == '200') {
		 
			$latitude = $geo_json['Placemark'][0]['Point']['coordinates'][0];
			$longitude = $geo_json['Placemark'][0]['Point']['coordinates'][1];
		?>

			<iframe width="<?php echo $iframe_width; ?>" height="<?php echo $iframe_height; ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="
				http://maps.google.com/maps
				?f=q
				&amp;source=s_q
				&amp;hl=en
				&amp;geocode=
				&amp;q=<?php echo $address; ?>
				&amp;aq=0
				&amp;ie=UTF8
				&amp;hq=
				&amp;hnear=<?php echo $address; ?>
				&amp;t=m
				&amp;ll=<?php echo $longitude; ?>,<?php echo $latitude; ?>
				&amp;z=12
				&amp;iwloc=
				&amp;output=embed"></iframe>

		<?php
		} else { echo "<p>No Address Available</p>";}
	}
	
?>