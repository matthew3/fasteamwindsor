<?php
	require_once('../recaptchalib.php');
	$privatekey = "6LfL2-MSAAAAAI1WjQmdFFQ6eY7qv66ouNCJnJZm";
	$resp = recaptcha_check_answer ($privatekey,
	                                $_SERVER["REMOTE_ADDR"],
	                                $_POST["recaptcha_challenge_field"],
	                                $_POST["recaptcha_response_field"]);
	
	if (!$resp->is_valid) {
	    // What happens when the CAPTCHA was entered incorrectly
	    echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/request_services.php?error=1&msg=Sorry,%20the%20RECaptcha%20failed. Please%20enter%20it%20correctly.")</script>';
	  } else {
	  
	    // Your code here to handle a successful verification
	    
	    //email application info from $_POST to $send_to
	    
	    include('class.phpmailer.php');
	    
	    $send_to = "mike.fehr@redcross.ca";
	    $subject = "WEBMAIL: FASTeam Request - " . $_POST['organization'] . "(" . $_POST['start_date'] . ")";
	    
	    $message = "A request was made on the FASTeam website:\r\n\r\n";
	    $message .= "Event Name: " . $_POST['event_name'] . " " . $_POST['last'] . "\r\n";
	    $message .= "Organization: " . $_POST['organization'] . "\r\n";
	    $message .= "Address: " . $_POST['address'] . "\r\n";
	    $message .= "Contact Name: " . $_POST['contact_name'] . "\r\n";
	    $message .= "Phone Number: " . $_POST['contact_phone'] . "\r\n\r\n";
	    $message .= "Email: " . $_POST['contact_email'] . "\r\n";
	    $message .= "Fax: " . $_POST['contact_fax'] . "\r\n";
	    $message .= "Event Start Date: " . $_POST['start_date'] . "\r\n";
	    $message .= "Comments:\r\n" . $_POST['comments'] . "\r\n";
	    
	    $email = new PHPMailer();
	    
	    $email->From = "noreply@fasteamwindsor.org";
	    $email->FromName = "FASTeam Website";
	    $email->Subject = $subject;
	    $email->Body = $message;
	    $email->AddAddress($send_to);
	   
	    $email->Send();
	    
	    echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/request_services.php?msg=Your%20request%20was%20successfully%20sent!%20Please%20wait%20for%20contact%20from%20the%20FASTeam.")</script>';
		
	  }
?>