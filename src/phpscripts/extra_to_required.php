<?php
	session_start();
	
	include('phpfunctions.php');
	
	if (!checkIfAdmin() and ($_POST['user_id'] != $_SESSION['user_id']))
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	$link = openDatabase();
	
	$user_id = $_POST['user_id'];
	$shift_id = $_POST['selected_shift_id'];
	$comment = $link->real_escape_string($_POST['user_comment']);
	
	//change sign_up to required
	$statement = "UPDATE sign_ups SET required=1 WHERE shift_id=" . $shift_id . " AND user_id=" . $user_id;
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		
		if (!$ok)
			echo "Opps: ". $link->error;
		else
		{
			$statement = "UPDATE shifts SET current_amount=current_amount+1 WHERE shift_id=" . $shift_id;
			$q = $link->prepare($statement);
			if ($q)
			{
				$ok = $q->execute();
				if (!$ok)
					echo "Opps: ". $link->error;
				$q->close();
			}
			else
				echo "query 4 error: " . $link->error;
		}
		$query->close();
	}
	else
		echo "query 1 error: " . $link->error;
	
	$link->close();
	
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>