<?php
	session_start();
	
	include('phpfunctions.php');
	
	if (!checkIfAdmin())
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	$link = openDatabase();
	
	$statement = "DELETE FROM meetings WHERE meeting_id=" . $_POST['meeting_id'];

	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if (!$ok)
			echo "Some sort of error occurred. Contact Admin.";
	}
	
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>