<?php
	session_start();
	
	include('phpfunctions.php');
	
	if (!checkIfAdmin())// and ($_POST['user_id'] != $_SESSION['user_id']))
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	$link = openDatabase();
	
	if (isset($_POST['selected_event_id']))
		$_SESSION['selected_event_id'] = $_POST['selected_event_id'];
	
	//delete the sign_up from the list
	$statement = "DELETE FROM sign_ups WHERE shift_id=" . $_POST['selected_shift_id'] . " AND user_id=" . $_POST['user_id'];
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
	}
	
	//decrement the current_amount field of the shift
	$statement = "UPDATE shifts SET current_amount=current_amount-1 WHERE shift_id=" . $_POST['selected_shift_id'];
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
	}
	
	//if the sign_up was a required one, move one of the extra members to the required field.
	//OR provide the users an option to move their own name over
	//OR provide coordinator with option to move extra member over
	
	$link->close();
	
	if ($ok)
		header("Location: " . $_SERVER['HTTP_REFERER']);
	else
	{
		echo "Sorry. There was some error removing the sign up."
		?>
			<br />
			<button onclick="history.go(-1);">Go Back</button>
		<?php
	}
?>