<?php
	session_start();
	
	//width:308
	//height:173
	
	include('phpfunctions.php');

	$filepath = '';

	$filepath = "files/" . $_FILES['file']['name'];
	
	while (file_exists($filepath))
	{
		$pos = strrpos($_FILES['file']['name'], ".");
		$_FILES['file']['name'] = substr($_FILES['file']['name'], 0, $pos) . "(2)" . substr($_FILES['file']['name'], $pos);
		$filepath = "files/" . $_FILES['file']['name'];
	}
	
	$filepath = str_replace('+', '\\+', $filepath);

	$link = openDatabase();
	
	$comment = $_POST['comment'];
	
	$pattern = '@width="\d+"@';
	$replacement = 'width="308"';
	$comment = preg_replace($pattern, $replacement, $comment);

	$pattern = '@height="\d+"@';
	$replacement = 'height="176"';
	$comment = preg_replace($pattern, $replacement, $comment);

	$statement = "INSERT INTO training_files VALUES (DEFAULT(file_id), '" . $_POST['title'] . "', '" . $comment . "', '" . $filepath . "', '" . $_FILES['file']['name'] . "')";

	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if (!$ok)
			echo "Some sort of error occurred. Ask Admin: " . $link->error;
		else if (!empty($filepath))
			move_uploaded_file($_FILES["file"]["tmp_name"], $filepath);
			
		$query->close();
	}
	
	$link->close();
	
	header('location: ' . $_SERVER['HTTP_REFERER']);
?>