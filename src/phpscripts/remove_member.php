<?php
	session_start();

	include('phpfunctions.php');
	
	if (!checkIfAdmin())
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	$_SESSION['any_errors'] = false;
	$_SESSION['remove_event_errors'] = '';
	
	function addError($message)
	{
		$_SESSION['remove_event_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	$member_id = $_POST['selected_member_id'];
	
	$link = openDatabase();
	
	//delete the event
	$statement = "DELETE FROM members WHERE user_id=" . $member_id;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if ($ok)
		{
			$_SESSION['any_errors'] = false;
			$_SESSION['remove_event_message'] = 'This member was successfully removed!';
		}
		else
			addError("There was an execute error: " . $link->error);
			
		$query->close();
	}
	else
		addError("Error with the query: 1");
		
	//select every shift that they were part
	$statement = "SELECT shift_id FROM sign_ups WHERE user_id=" . $member_id;
	$ok = false;
	
	$shifts = array();
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$query->bind_result($shift_id);
		$query->execute();
		while ($query->fetch())
		{
			array_push($shifts, $shift_id);
		}
		$query->close();
	}
	
	//delete every sign_up associated with this shift
	$statement = "DELETE FROM sign_ups WHERE user_id=" . $member_id;
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		$query->close();
	}
	
	//delete every sign_up associated with this shift
	$statement = "UPDATE shifts SET current_amount = current_amount - 1 WHERE shift_id IN (" . implode(',',$shifts) . ')';
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		$query->close();
	}
		
	$link->close();

	header('Location: ' . $_SERVER['HTTP_REFERER']);
	exit();
?>