<?php
	session_start();
	
	date_default_timezone_set('Canada/Eastern');
	
	include("phpfunctions.php");
	
	function randSalt()
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$retStr = "$2a$10$";
		for ($i = 0; $i < 22; $i++)
		{
			$retStr .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $retStr;
	}
	
	function addError($error)
	{
		$_SESSION['any_errors'] = true;
		$_SESSION['change_info_errors'] .= $error . "<br />";
	}
	
	$_SESSION['any_errors'] = false;
	$_SESSION['change_info_errors'] = '';
	
	if (isset($_POST['change_info']))
	{
		$user = $_POST['user'];
		$email = $_POST['email'];
		$phone_home = $_POST['phone_home'];
		$phone_cell = $_POST['phone_cell'];
		
		$textable = 0;
		if (isset($_POST['textable']))
			$textable = 1;
		
		if ($email === '')
			addError("You can not have no email. Please enter an email.");
		if ($user === '')
			addError("You can not have no username. Please enter a username.");
		
		if (!$_SESSION['any_errors'])
		{
			$link = openDatabase();
			//check to see if email or username already exist
		
			$statement = "UPDATE members SET user='" . $user . "', email='" . $email . "', phone_home='" . $phone_home . "', phone_cell='" . $phone_cell . "', textable=" . $textable . " WHERE user_id=" . $_SESSION['user_id'];
			$query = $link->prepare($statement);
			
			if ($query)
			{
				$ok = $query->execute();
				if ($ok)
				{
					//log the password change
					insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "Change Info: Changed information.", date('Y-m-d H:i:s'));
					$_SESSION['change_info_message'] = "The changes were successfully added!";
				}
				else
					addError("Zero rows were affected in the database: please contact the administrator: " . $link->errno . $link->error);
				
				$query->close();
			}
			else
				addError("Opps, contact administrator: " . $link->errno . $link->error);
			
			$link->close();
		}
	}
	else if (isset($_POST['change_pass']))
	{
		$old_pass = $_POST['old_pass'];
		$pass = $_POST['pass'];
		$repass = $_POST['repass'];
		
		$user = $_SESSION['user'];
		
		$link = openDatabase();
		
		//escape the input
		$old_pass = $link->real_escape_string($old_pass);
		$old_pass = addcslashes($old_pass, '%_');
		
		$pass = $link->real_escape_string($pass);
		$pass = addcslashes($pass, '%_');
		
		$repass = $link->real_escape_string($repass);
		$repass = addcslashes($repass, '%_');
		
		//make sure input is correct
		if (strlen($pass) < 3)
			addError("Your password must be greater than 3 characters.");
		else if ($pass != $repass)
			addError("The passwords did not match, try again.");
		else
		{
			$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$^&*';
			for ($i = 0; $i < strlen($pass) && !$_SESSION['any_errors']; $i++)
			{
				if (!strpbrk($chars, $pass{$i}))
				{
					addError('Only these symbols are allowed for the password: ' . $chars);
				}
			}
		}
		
		//check that password is not already in the database for this user
		$statement = "SELECT user, pass, salt FROM members WHERE user='" . $user . "'";// OR email='" . $email . "'";
		$result = queryWithAssocResult($statement);
		
		foreach ($result as $row)
		{
			$cryptPass = crypt($pass, $row['salt']); //find encrypted password
			
			if ($row['pass'] === $cryptPass)
			{
				addError('There was an error with your password, please try a different one.');
			}
		}
		
		if (!$_SESSION['any_errors'])
		{	
			$statement = "SELECT pass, salt FROM members WHERE user_id=" . $_SESSION['user_id'];
			$query = $link->prepare($statement);
		
			if ($query)
			{
				$query->bind_result($actual_pass, $salt);
				$query->execute();
				if ($query->fetch())
				{
					$query->close();
					
					global $old_pass;
					$old_pass = crypt($old_pass, $salt);
					if ($old_pass != $actual_pass)
						addError("Sorry, the old password you entered was not correct, please try again.");
					else
					{
						//set the new password and salt after encryption
						$new_salt = randSalt();
						$pass = crypt($pass, $new_salt);
						
						$statement = "UPDATE members SET pass='" . $pass . "', salt='" . $new_salt . "' WHERE user_id=" . $_SESSION['user_id'];
						$query = $link->prepare($statement);
						
						if ($query)
						{
							if (!$query->execute())
								addError("Something wrong happened when trying to update, please contact Administrator. <br /> Error: " . $link->errno . $link->error);
						
							if (mysqli_affected_rows($link) > 0)
							{
								$_SESSION['change_info_message'] = "The changes were successfully added!";
								
								//log the password change
								insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "Change Info: Changed password.", date('Y-m-d H:i:s'));
							}
							else
								addError("Zero rows were affected in the database: please contact the administrator.");
						
							$query->close();
						}
						else
							addError("Sorry, there was a database error: " . $link->errno . $link->error);
					}
				}
				else
					addError("Sorry, your user_id was not found in the database, please contact the administrator.");
			}
			else
				addError("There was a query error: " . $link->errno . $link->error);
			
			$link->close();
		}
	}
	
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>