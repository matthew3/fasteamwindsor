<?php
	session_start();
	
	include('phpfunctions.php');
	
	if (!checkIfAdmin())
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	function addError($message)
	{
		$_SESSION['manage_shift_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	$link = openDatabase();
	
	$selected_shift_id = $_POST['selected_shift_id'];
	
	//remove shift
	$statement = "DELETE FROM shifts WHERE shift_id=" . $_POST['selected_shift_id'];
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if ($ok)
		{
			$_SESSION['manage_shift_message'] = "Action successfully completed.<br />";
			$_SESSION['any_errors'] = false;
		}
		else
			addError("Query messed up, ask the Admin.");
		$query->close();
	}
		
	//remove any sign_up associated with that shift
	$statement = "DELETE FROM sign_ups WHERE shift_id=" . $selected_shift_id;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if ($ok)
		{
			$_SESSION['manage_shift_message'] = "Action successfully completed.<br />";
			$_SESSION['any_errors'] = false;
		}
		else
			addError("Query messed up, ask the Admin.");
		$query->close();
	}
	
	$link->close();
		
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>