<?php
	session_start();
	
	include('phpfunctions.php');
	
	if (!checkIfAdmin())
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	if ($_FILES['file']['error'] > 0)
	{
		echo $_FILES['file']['error'];
		echo "Sorry there was an error for some reason, please contact the admin.";
	}
	else
	{
		
		$link = openDatabase();
		
		$statement = "SELECT filename FROM training_files WHERE file_id=" . $_POST['file_id'];
	
		$query = $link->prepare($statement);
		if ($query)
		{
			$query->bind_result($filename);
			$query->execute();
			$query->fetch();
			
			$query->close();
		}
		
		$statement = "DELETE FROM training_files WHERE file_id=" . $_POST['file_id'];
	
		$query = $link->prepare($statement);
		if ($query)
		{
			$ok = $query->execute();
			
			$filepath = "files/" . $filename;
			
			if ($ok && file_exists($filepath))
			{
				unlink($filepath);
			}
			
			$query->close();
		}
		
		$link->close();
		
		header('location: ' . $_SERVER['HTTP_REFERER']);
	}
?>