<?php
	session_start();

	include('phpfunctions.php');
	
	
	//save all form data to repopulate on failure
	$_SESSION['form_data_saved'] = true;
	$_SESSION['saved_form_data'] = $_POST;
	
	$textable = 0;
	if (isset($_POST['textable']))
	{
		$textable = 1;
		$_SESSION['saved_form_data']['textable'] = 1;
	}
	//end of saving
	
	$user_id = $_POST['user_id'];
	$first = $_POST['first'];
	$last = $_POST['last'];
	$user = $_POST['user'];
	
	$pass = $_POST['pass'];
	$confirm_pass = $_POST['confirm_pass'];
	$salt = $_POST['salt'];
	
	$email = $_POST['email'];
	$phone_cell = $_POST['phone_cell'];
	$phone_home = $_POST['phone_home'];
	$textable = 0;
	if (isset($_POST['textable']))
		$textable = $_POST['textable'];
		
	$position = $_POST['position'];
	$member_since = $_POST['member_since'];
	$status = $_POST['status'];
	
	$address = $_POST['address'];
	
	$_SESSION['update_member_errors'] = "";
	$_SESSION['any_errors'] = false;
	
	$member_since = $_POST['member_since'];
	$_SESSION['form_status'] = $status;
	
	if ($user_id === 0)
	{
		$_SESSION['update_member_errors'] .=  "Sorry, there was a problem updating, please use the 'select member' functionality before you update a member. <br />";
		$_SESSION['any_errors'] = true;
	}
	
	if ($first === '')
	{
		$_SESSION['update_member_errors'] .= "Missing a first name, please provide one. <br />";
		$_SESSION['any_errors'] = true;
	}
	if ($last === '')
	{
		$_SESSION['update_member_errors'] .= "Missing a last name, please provide one. <br />";
		$_SESSION['any_errors'] = true;
	}
	if ($email === '')
	{
		$_SESSION['update_member_errors'] .= "Missing an email, please provide one. <br />";
		$_SESSION['any_errors'] = true;
	}
	
	$statement_addition = "";
	if ($pass != $confirm_pass)
	{
		$_SESSION['update_member_errors'] .= "Passwords do not match. <br />";
		$_SESSION['any_errors'] = true;
	}
	else if (!($pass === ''))
	{
		//check that password is not already in the database
		$statement = "SELECT user, pass, salt FROM members WHERE user='" . $user . "'";// OR email='" . $email . "'";
		$result = queryWithAssocResult($statement);
		
		foreach ($result as $row)
		{
			$cryptPass = crypt($pass, $row['salt']); //find encrypted password
			
			if ($row['pass'] === $cryptPass)
			{
				$_SESSION['update_member_errors'] .=  "Some other member already has this username and password combination. Please choose a different password for this user.<br />";
				$_SESSION['any_errors'] = true;
			}
		}
		
		$statement_addition = "', pass='" . crypt($pass, $salt);
	}
	
	if (!$_SESSION['any_errors'])
	{
		$link = openDatabase();
		
		//statement addition is adding a password if there was one
		$statement = "UPDATE members SET first='" . $first . "', last='" . $last . "', user='" . $user . $statement_addition . "', email='" . $email . "', phone_cell='" . $phone_cell . "', phone_home='" . $phone_home . "', textable='" . $textable . "', position='" . $position . "', member_since='" . $member_since . "', status='" . $status . "', address='" . $address . "' WHERE user_id=" . $user_id;
		$query = $link->prepare($statement);
		
		if ($query)
		{
			$query->execute();
			if ($link->errno)
			{
				$_SESSION['update_member_errors'] .= "Sorry, there was some sort of error, contact the admin: " . $link->errno . $link->error . "<br />";
				$_SESSION['any_errors'] = true;
			}
			else
			{
				$_SESSION['update_member_message'] = "The changes to " . $first . " " . $last . " were successfully completed!<br />";
				$_SESSION['any_errors'] = false;
			}
			$query->close();
		}
		else
			echo $link->error;
		$link->close();
	}

	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>