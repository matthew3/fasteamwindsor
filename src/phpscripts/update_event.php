<?php
	session_start();
	
	include('phpfunctions.php');
	
	$_SESSION['new_event_errors'] = '';
	$_SESSION['any_errors'] = false;
	
	function addError($message)
	{
		$_SESSION['new_event_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	
	$link = openDatabase();
	
	$event_id = $_POST['event_id'];

	$event_name = $link->real_escape_string($_POST['event_name']);
	$organization = $link->real_escape_string($_POST['organization']);
	$address = $link->real_escape_string($_POST['address']);
	//$map_link = $_POST['event_name'];
	$contact_name = $link->real_escape_string($_POST['contact_name']);
	$contact_phone = $link->real_escape_string($_POST['contact_phone']);
	$contact_fax = $link->real_escape_string($_POST['contact_fax']);
	$contact_email = $link->real_escape_string($_POST['contact_email']);
	$start_date = $link->real_escape_string($_POST['start_date']);
	$map_link = $link->real_escape_string($_POST['map_link']);
	$comments = $link->real_escape_string($_POST['comments']);
	
	if ($start_date === '')
		addError("A start date needs to be provided in format yyyy-mm-dd");
	
	if (!$_SESSION['any_errors'])
	{
		$statement = "UPDATE events SET event_name='" . $event_name . "', organization='" . $organization . "', address='" . $address . "', contact_name='" . $contact_name . "', contact_phone='" . $contact_phone . "', contact_fax='" . $contact_fax . "', contact_email='" . $contact_email . "', start_date='" . $start_date . "', map_link='" . $map_link . "', comments='" . $comments . "' WHERE event_id=" . $event_id;
		$query = $link->prepare($statement);
		
		if ($query)
		{
			$ok = $query->execute();
			
			if ($ok)
			{
				$_SESSION['new_event_message'] = "Your event was updated successfully!";
				$_SESSION['any_errors'] = false;
			}
			else
				addError("There was some error executing the query, please contact Admin: " . $link->error);
			$query->close();
		}
		else
			addError("Something went wrong with query 1. Please contact Admin.");

			echo $link->error;
			
		$link->close();
	}
		
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>