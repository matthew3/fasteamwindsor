<?php
	session_start();
	
	include('phpfunctions.php');
	
	$link = openDatabase();
	
	$event_id = $_POST['event_id'];
	$date = $link->real_escape_string($_POST['date']);
	$start = $link->real_escape_string($_POST['start_time']);
	$end = $link->real_escape_string($_POST['end_time']);
	$required_amount = $link->real_escape_string($_POST['required_amount']);
	
	//configure times if entered as integers (10 and 16) rather than times (10:00 and 16:00) and 1000 as 10:00
	if (strlen($start) < 3)
		$start = $start . ":00";
	else if (strlen($start) == 3)
	{
		$start = $start[0] . ":" . substr($start, 1);
	}
	else if (strlen($start) == 4)
	{
		$start = $start[0] . $start[1] . ":" . substr($start, 2);
	}
	
	if (strlen($end) < 3)
		$end = $end . ":00";
	else if (strlen($end) == 3)
	{
		$end = $end[0] . ":" . substr($end, 1);
	}
	else if (strlen($end) == 4)
	{
		$end = $end[0] . $end[1] . ":" . substr($end, 2);
	}
	
	$statement = "INSERT INTO shifts () VALUES(DEFAULT(shift_id), " . $event_id . ", '" . $date . "', '" . $start . "', '" . $end . "', " . $required_amount . ", 0)";

	$query = $link->prepare($statement);
	
	if ($query)
	{
		$ok = $query->execute();
		if ($ok)
		{
			$_SESSION['new_shift_message'] = "Successfully added shift.";
			$_SESSION['any_errors'] = false;
		}
		else
		{
			$_SESSION['new_shift_errors'] = "There was a problem with the query: " . $link->error;
			$_SESSION['any_errors'] = true;
		}
	}
	
	header('location: http://www.fasteamwindsor.org/event_management_page.php');
?>