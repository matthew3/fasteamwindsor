<?php
	session_start();

	include('phpfunctions.php');
	
	function randSalt()
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$retStr = "$2a$10$";
		for ($i = 0; $i < 22; $i++)
		{
			$retStr .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		return $retStr;
	}
	
	function addError($message)
	{
		$_SESSION['new_member_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	if (!isset($_POST) || !isset($_SESSION))
		addError("No data to work with, and/or no session available.");
	
	$first = $_POST['first'];
	$last = $_POST['last'];
	$user = $_POST['user'];
	$pass = $_POST['pass'];
	$repass = $_POST['confirm_pass'];
	$email = $_POST['email'];
	$phone_home = $_POST['phone_home'];
	$phone_cell = $_POST['phone_cell'];
	$position = $_POST['position'];
	$member_since = $_POST['member_since'];
	$status = $_POST['status'];
	$address = $_POST['address'];
	
	$_SESSION['form_data_saved'] = true;
	$_SESSION['saved_form_data'] = $_POST;
	
	$textable = 0;
	if (isset($_POST['textable']))
	{
		$textable = 1;
		$_SESSION['saved_form_data']['textable'] = 1;
	}
	
	$member_since = $_POST['member_since'];
	$_SESSION['form_status'] = $status;
	
	//open link to database
	$link = openDatabase();
	
	//escape the input, to protect sql database
	$first = $link->real_escape_string($first);
	$last = $link->real_escape_string($last);
	$user = $link->real_escape_string($user);
	$pass = $link->real_escape_string($pass);
	$email = $link->real_escape_string($email);
	$phone_home = $link->real_escape_string($phone_home);
	$phone_cell = $link->real_escape_string($phone_cell);
	
	$first = addcslashes($first, '%_'); //protects against some 'LIKE' issues with % and _ protection
	$last = addcslashes($last, '%_');
	$user = addcslashes($user, '%_');
	$email = addcslashes($email, '%_');
	$phone_home = addcslashes($phone_home, '%_');
	$phone_cell = addcslashes($phone_cell, '%_');
	//end of escaping

	//do the input checking
	
	$_SESSION['new_member_errors'] = '';
	$_SESSION['any_errors'] = false;
	
	//cap first letters
	if ($first === '')
	{
		addError('Missing a first name.');
	}
	$first = strtolower($first);
	$first = ucfirst($first);
	
	if ($last === '')
	{
		addError('Missing a last name.');
	}
	$last = strtolower($last);
	$last = ucfirst($last);
	
	if ($email === '')
	{
		addError('Missing an email.');
	}
	
	if ($repass != $pass)
	{
		addError('The passwords do not match.');
	}
	if (strlen($pass) < 3)
	{
		addError('The password needs to be at least 3 characters.');
	}
	
	$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$^&*';
	for ($i = 0; $i < strlen($pass) && !$_SESSION['any_errors']; $i++)
	{
		if (!strpbrk($chars, $pass{$i}))
		{
			addError('Only these symbols are allowed for the password: ' . $chars);
		}
	}
	
	//now I can add the slashes to the password
	$pass = addcslashes($pass, '%_');
	
	//check that password is not already in the database for this user
	$statement = "SELECT user, pass, salt FROM members WHERE user='" . $user . "'";// OR email='" . $email . "'";
	$result = queryWithAssocResult($statement);
	
	foreach ($result as $row)
	{
		$cryptPass = crypt($pass, $row['salt']); //find encrypted password
		
		if ($row['pass'] === $cryptPass)
		{
			addError('Some other member already has this username and password combination. Please choose a different password for this user.');
		}
	}
	
	//end of input checking, should I go back to previous page with errors?
	//continue only if there are no errors
	if (!$_SESSION['any_errors'])
	{
		$salt = randSalt();
		$pass = crypt($pass, $salt); //create the encrypted password
		
		//Check for username or email doubles
		$selection = $link->prepare("SELECT email FROM members WHERE email='" .$email . "'");
		if (!$selection)
			addError('Could not perform database operation: ' . $link->errno . ": " . $link->error);
		else
		{
			$selection->execute();
			$selection->bind_result($found_email);
			$selection->fetch();
			
			//check if email double
			if ($found_email === $email)
			{
				addError('This email already exists!<br />');
			}
			
			$selection->close();
		}
		if (!$_SESSION['any_errors'])
		{
			//prepare the insert operation
			$query = $link->prepare("INSERT INTO members () VALUES (DEFAULT(user_id), '". $first . "', '" . $last . "', '" . $email . "', '" . $user . "', '" . $pass . "', '" . $salt . "', '" .
																	$phone_home . "', '" . $phone_cell . "', " . $textable . ", '" . $position . "', '" . $member_since . "', '" . $status . "', '" . $address . "', 0)");
	
			if ($query)
			{
				//execute the insert operation
				$ok = $query->execute();
				
				if ($ok)//if no database errors
				{
					$_SESSION['new_member_message'] = "member '" . $first . " " . $last . "' added successfully!";
					$_SESSION['any_errors'] = false;
				}
				else
				{
					addError("There was an error inserting: " . $link->error);
				}
					
			}
			else
			{
				addError("There was a query error: " . $link->errno . " " . $link->error);
			}
		}
		
	}
	
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>