<?php
	session_start();
	
	date_default_timezone_set('Canada/Eastern');
	
	include('phpfunctions.php');
	
	$link = openDatabase();
	
	$event_id = $_POST['event_id'];
	$shift_id = $_POST['shift_id'];
	$selected_user_id = $_SESSION['user_id'];
	
	if (isset($_POST['selected_user_id']))
		if ($_POST['selected_user_id'] != 0)
			$selected_user_id = $_POST['selected_user_id'];
	
	$comment = $link->real_escape_string($_POST['user_comment']);
	
	if ($comment === 'Enter a comment.')
		$comment = '';
	
	//get shift_required and current_amount
	$statement = "SELECT required_amount, current_amount FROM shifts WHERE shift_id=" . $shift_id;
	$query = $link->prepare($statement);
	if ($query)
	{
		$query->bind_result($required_amount, $current_amount);
		$query->execute();
		$query->fetch();
		$query->close();
	}
	else
		echo "query 1 error: " . $link->error;
	
	if ($required_amount > $current_amount)
		$required = 1;
	else
		$required = 0;
		
	//make sure user is not already signed up
	$statement = "SELECT user_id FROM sign_ups WHERE user_id=" . $selected_user_id . " AND shift_id=" . $shift_id;
	$retArray = queryWithAssocResult($statement);
	if (sizeof($retArray) > 0)
	{
		$_SESSION['sign_up_errors'] = "That person is already signed up!";
	}
	else
	{
		//make a new sign_up entry
		$statement = "INSERT INTO sign_ups () VALUES(" . $selected_user_id . ", " . $event_id . ", " . $shift_id . ", '" . $comment . "', " . $required . ")";
		$query = $link->prepare($statement);
		if ($query)
		{
			$ok = $query->execute();
			
			if (!$ok)
				echo "Opps: ". $link->error;
			else
			{
				//insert a record into the log
				insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "Event sign up: (id " . $event_id . ")", date('Y-m-d H:i:s'));
			}
				
			$query->close();
		}
		else
			echo "query 3 error: " . $link->error;
		
		//if user is required, add one to the current amount
		if ($required == 1)
		{
			$statement = "UPDATE shifts SET current_amount=current_amount+1 WHERE shift_id=" . $shift_id;
			$query = $link->prepare($statement);
			if ($query)
			{
				$ok = $query->execute();
				if (!$ok)
					echo "Opps: ". $link->error;
				$query->close();
			}
			else
				echo "query 4 error: " . $link->error;
		}
	}
	
	$link->close();
	
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>