<?php 
	session_start();
	
	include("phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
	
    	$fullPath = "files/" . $_GET['filename'];
	
	if (isset($_SESSION['user_id']))
	{
		//insert a record into the log
		insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "File download: " . $_GET[filename], date('Y-m-d H:i:s'));

	    if (file_exists($fullPath)) {       
	        header('Content-Description: File Transfer'); 
	        header('Content-Type: application/octet-stream'); 
	        header('Content-Length: ' . filesize($fullPath)); 
	        header('Content-Disposition: attachment; filename="' . $_GET['filename'] . '"'); 
	
	        readfile($fullPath);
	        exit();
	    }
	}
	else
		//please login

    exit();

?>