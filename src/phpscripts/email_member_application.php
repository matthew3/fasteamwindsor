<?php
	require_once('../recaptchalib.php');
	$privatekey = "6LfL2-MSAAAAAI1WjQmdFFQ6eY7qv66ouNCJnJZm";
	$resp = recaptcha_check_answer ($privatekey,
	                                $_SERVER["REMOTE_ADDR"],
	                                $_POST["recaptcha_challenge_field"],
	                                $_POST["recaptcha_response_field"]);
	
	if (!$resp->is_valid) {
	    // What happens when the CAPTCHA was entered incorrectly
	    echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/apply_to_join.php?error=1&msg=Sorry,%20the%20RECaptcha%20failed. Please%20enter%20it%20correctly.")</script>';
	  } else {
	  
	    // Your code here to handle a successful verification
	    
	    //email application info from $_POST to $send_to
	    
	    include('class.phpmailer.php');
	    
	    $send_to = "mike.fehr@redcross.ca";
	    $subject = "WEBMAIL: FASTeam Application - " . $_POST['last'];
	    
	    $message = "An application was filled out on the FASTeam website:\r\n\r\n";
	    $message .= "Name: " . $_POST['first'] . " " . $_POST['last'] . "\r\n";
	    $message .= "Email: " . $_POST['email'] . "\r\n";
	    $message .= "Address: " . $_POST['address'] . "\r\n";
	    $message .= "Home Phone: " . $_POST['phone_home'] . "\r\n";
	    $message .= "Cell Phone: " . $_POST['phone_cell'] . "\r\n\r\n";
	    
	    $message .= "Attached should be the application and a resume.";
	    
	    $email = new PHPMailer();
	    
	    $email->From = "noreply@fasteamwindsor.org";
	    $email->FromName = "FASTeam Website";
	    $email->Subject = $subject;
	    $email->Body = $message;
	    $email->AddAddress($send_to);
	    
	    if(!file_exists($_FILES['application']['tmp_name']) || !is_uploaded_file($_FILES['resume']['tmp_name'])) {
		    echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/apply_to_join.php?error=1&msg=Please%20be%20sure%20to%20upload%20your%20application%20and%20resume!%20Thank%20you.")</script>';
		}
	    if ($_FILES['application']['error'] > 0 || $_FILES['resume']['error'] > 0)
	    {
	    	echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/apply_to_join.php?error=1&msg=Sorry,%20the%20file%20upload%20failed%20for%20unkown%20reasons.%20Please%20try%20again.")</script>';
	    }
	    
	    $applicationPath = "application_upload/" . $_FILES['application']['name'];
	    $resumePath = "application_upload/" . $_FILES['resume']['name'];
	    move_uploaded_file( $_FILES['application']['tmp_name'], $applicationPath);
	    move_uploaded_file( $_FILES['resume']['tmp_name'], $resumePath);
	    
	    $email->AddAttachment( $applicationPath , $_FILES['application']['name'] );
	    $email->AddAttachment( $resumePath , $_FILES['resume']['name'] );
	    
	    $email->Send();
	    
	    echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/apply_to_join.php?msg=Your%20application%20was%20successfully%20sent!%20Please%20wait%20for%20contact%20from%20the%20FASTeam.")</script>';
		
	  }
?>