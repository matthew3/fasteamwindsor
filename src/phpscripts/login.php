<?php
	include("phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');

	$user = $_POST['user'];
	$inputPass = $_POST['pass'];
	
	$link = openDatabase();
	
	//escape the input, to protect sql database
	$user = $link->real_escape_string($user);
	$pass = $link->real_escape_string($pass);
	
	$link->close();
	
	$user = addcslashes($user, '%_'); //protects against some 'LIKE' issues with % and _ protection
	$pass = addcslashes($pass, '%_');
	//end of escaping
	
	$statement = "SELECT user_id, user, first, last, email, position, pass, salt FROM members WHERE user='" . $user . "'";
	
	$result = queryWithAssocResult($statement);
	
	$found = false;
	
	foreach ($result as $row)
	{
		if (!$found)
		{
			$inputCrypt = crypt($inputPass, $row['salt']); //find encrypted password
			
			if ($row['pass'] == $inputCrypt) //compare both encrypted data
			{
				$found = true;
				//PASS
				//start new session?
				//include('logout.php');
				session_start();
				
				$_SESSION['user_id'] = $row['user_id'];
				$_SESSION['user'] = $row['user'];
				$_SESSION['first'] = $row['first'];
				$_SESSION['last'] = $row['last'];
				$_SESSION['email'] = $row['email'];
				unset($_SESSION['position']); //Weird position bug
				$_SESSION['position'] = $row['position'];
				
				//insert a record into the log
				insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "Member log in.", date('Y-m-d H:i:s'));
				
				//redirect to index
				header('Location: http://www.fasteamwindsor.org', true, 302);
				exit;
			}
		}
	}
	
	if (!$found)
	{
		echo '<script type="text/javascript">window.location.replace("http://www.fasteamwindsor.org/login_error.php?msg=I\'m%20Sorry,%20the%20information%20entered%20was%20not%20correct.%20Please%20try%20again.")</script>';
	}
?>