<?php
	session_start();
	
	include("phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');

	//insert a record into the log
	insertIntoLog($_SESSION['first'] . " " . $_SESSION['last'], "Member log out.", date('Y-m-d H:i:s'));

	$_SESSION = array();
	
	if (ini_get("session.use_cookies"))
	{
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
					$params['path'], $params['domain'],
					$params['secure'], $params['httponly']);
	}
	
	session_destroy();
	
	header('Location: http://www.fasteamwindsor.org');
?>