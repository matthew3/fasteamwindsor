<?php
	session_start();

	include('phpfunctions.php');
	
	if (!checkIfAdmin())
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	
	$_SESSION['any_errors'] = false;
	$_SESSION['remove_event_errors'] = '';
	
	function addError($message)
	{
		$_SESSION['remove_event_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	$event_id = $_POST['selected_event_id'];
	
	$link = openDatabase();
	
	//delete the event
	$statement = "DELETE FROM events WHERE event_id=" . $event_id;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		if ($ok)
		{
			$_SESSION['any_errors'] = false;
			$_SESSION['remove_event_message'] = 'This event was successfully removed!';
		}
		else
			addError("There was an execute error: " . $link->error);
			
		$query->close();
	}
	else
		addError("Error with the query: 1");
		
	//delete every shift associated with this event
	$statement = "DELETE FROM shifts WHERE event_id=" . $event_id;
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
	}
	
	//delete every sign_up associated with this shift
	$statement = "DELETE FROM sign_ups WHERE event_id=" . $event_id;
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
	}
	
	//delete all files that are associated with the event
		//physical files
	$statement = "SELECT filename FROM event_files WHERE event_id=" . $event_id;
	$ok = false;
	
	$result = queryWithAssocResult($statement);
	foreach ($result as $filename)
	{
		$filename = "files/" . $filename;
		if (file_exists($filename))
		{
			unlink($filename);
		}
	}
	
	//delete database links
	$statement = "DELETE FROM event_files WHERE event_id=" . $event_id;
	$ok = false;
	
	$query = $link->prepare($statement);
	if ($query)
	{
		$ok = $query->execute();
		$query->close();
	}
		
	$link->close();

	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>