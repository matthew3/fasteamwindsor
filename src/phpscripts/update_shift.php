<?php
	session_start();
	
	include('phpfunctions.php');
	
	function addError($message)
	{
		$_SESSION['manage_shift_errors'] .= $message . "<br />";
		$_SESSION['any_errors'] = true;
	}
	
	$link = openDatabase();
	
	//update shift
	
	$selected_event_id = $_POST['selected_event_id'];
	$selected_shift_id = $_POST['selected_shift_id'];
	$date = $_POST['date'];
	$start_time = $_POST['start_time'];
	$end_time = $_POST['end_time'];
	$required = $_POST['required_amount'];
	
	$statement = '';
	
	
	$statement = "UPDATE shifts SET date='" . $date . "', start_time='" . $start_time . "', end_time='" . $end_time . "', required_amount='" . $required ."'
				WHERE shift_id=" . $selected_shift_id;
	
	if ($statement != '')
	{
		$query = $link->prepare($statement);
		if ($query)
		{
			$ok = $query->execute();
			if ($ok)
			{
				$_SESSION['manage_shift_message'] = "Action successfully completed.<br />";
				$_SESSION['any_errors'] = false;
			}
			else
				addError("Query messed up, ask the Admin.");
		}
	}
	else
		addError("No action selected, either try again or contact Admin.");
		
	header("Location: " . $_SERVER['HTTP_REFERER']);
?>