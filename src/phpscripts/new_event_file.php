<?php
	session_start();
	
	include('phpfunctions.php');
	
	$filepath = '';
	
	if (! ($_FILES['file']['error'] > 0))
	{
		$filepath = "files/" . $_FILES['file']['name'];
		
		while (file_exists($filepath))
		{
			$pos = strrpos($_FILES['file']['name'], ".");
			$_FILES['file']['name'] = substr($_FILES['file']['name'], 0, $pos) . "(2)" . substr($_FILES['file']['name'], $pos);
			$filepath = "files/" . $_FILES['file']['name'];
		}
		
		$filepath = str_replace('+', '\\\\+', $filepath);
	
		$selected_event_id = $_POST['selected_event_id'];
		
		$link = openDatabase();
			
		$statement = "INSERT INTO event_files VALUES (DEFAULT(file_id), " . $selected_event_id . ", '" . $_POST['title'] . "', '" . $_POST['comment'] . "', '" . $_FILES['file']['name'] . "')";
	
		$query = $link->prepare($statement);
		if ($query)
		{
			$ok = $query->execute();
			if (!$ok)
				echo "Some sort of error occurred. Ask Admin: " . $link->error;
			else if (!empty($filepath))
				move_uploaded_file($_FILES["file"]["tmp_name"], $filepath);
				
			$query->close();
		}
		
		$link->close();
	}
	else
		echo "There was an error: " . $_FILES['file']['error'];
	
	header('location: ' . $_SERVER['HTTP_REFERER']);
?>