<?php
	session_start();
	
	
	include("phpscripts/phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div class="clear"></div>
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner" style="text-align:center;">
				
						<?php
							if (isset($_GET['msg']))
							{
								if (isset($_GET['error']))
								{
									echo '<div class="errors">';
								}
								else
								{
									echo '<div class="success">';
								}
								echo $_GET['msg'];
								echo '</div> <br />';
							}
						?>
						
						<h2>Request Services</h2>
						
						<table width="100%">
							<tr>
								<td width="15%">&nbsp</td>
								<td width="76%">
									<div class="instructions">
										To inquire or book your event with the First Aid Services Team, please send an email below:<br /><br />
			
			As an alternative you can call 519-944-8144 between the hours of 8:30 a.m. - 4:30 p.m. Monday to Friday, excluding all government holidays, or fax a signed request to 519-974-1424.
									</div>
								</td>
							</tr>
						</table>
						
						<form name="event_form" method="post" action="phpscripts/email_request_services.php">
							<table align="center">
								<tr>
									<td> Event Name: </td>
									<td> <input name="event_name" type="text" /> </td>
								</tr>
								<tr>
									<td> Organization: </td>
									<td> <input name="organization" type="text" /> </td>
								</tr>
								<tr>
									<td> Address: </td>
									<td> <input name="address" type="text" /> </td>
								</tr>
								<tr>
									<td> Contact Name: </td>
									<td> <input name="contact_name" type="text" /> </td>
								</tr>
								<tr>
									<td> Contact Phone: </td>
									<td> <input name="contact_phone" type="text" /> </td>
								</tr>
								<tr>
									<td> Contact Fax: </td>
									<td> <input name="contact_fax" type="text" /> </td>
								</tr>
								<tr>
									<td> Contact Email: </td>
									<td> <input name="contact_email" type="text" /> </td>
								</tr>
								<tr>
									<td> Start Date (yyyy-mm-dd): </td>
									<td> <input name="start_date" type="text" /> </td>
								</tr>
								<tr>
									<td> Comments: </td>
									<td> <textarea name="comments" rows="4" cols="30"></textarea> </td>
								</tr>
								
								<tr>
									<td colspan="2" align="center">
									<!-- add captia -->
									<?php
										require_once('recaptchalib.php');
										$publickey = "6LfL2-MSAAAAAPxO6rsLB-ZcU3O5IhDA4Iy9jtDS"; // you got this from the signup page
										echo recaptcha_get_html($publickey);
		  							?>
		  							</td>
	  							</tr>
								
								<tr>
									<td colspan="2" align="center"> <input type="submit" value="Send Request" /> </td>
								</tr>
							</table>
						</form>
						
				</div> </div>
				<!-- END OF "body_sect" -->
				
			</div>
		</div> <!-- END OF MAIN DIV -->
		
	</body>
</html>