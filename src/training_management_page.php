<?php
	session_start();
	
	include("phpscripts/phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner">
				<?php
					if (checkIfAdmin() and ($_SESSION['last'] === 'Fehr' or $_SESSION['position'] === 'Admin'))
					{
						$monthAgo = date('Y-m-d H:i:s', strtotime('1 month ago'));
						$statement = "SELECT * FROM member_log WHERE timestamp > '" . $monthAgo . "' ORDER BY timestamp DESC";
						
						$result = queryWithAssocResult($statement);
						
						foreach ($result as $row)
						{
							echo implode(" | ",$row) . "<br />";
						}
					}
				?>
					
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
			</div>
		</div> <!-- END OF MAIN DIV -->
	</body>
</html>