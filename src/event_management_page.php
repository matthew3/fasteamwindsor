<?php
	//this page is meant for Coordinator to be able to manage all of the events
	session_start();
	
	
	include("phpscripts/phpfunctions.php");
	
	date_default_timezone_set('Canada/Eastern');
	
	$event_info = array('event_id' => 0,
						'event_name' => '',
						'organization' => '',
						'address' => '',
						'map_link' => '',
						'contact_name' => '',
						'contact_phone' => '',
						'contact_fax' => '',
						'contact_email' => '',
						'start_date' => '',
						'comments' => '');
	
	//form data was saved from a new member attempt, fill the inputs with it
	if (isset($_SESSION['form_data_saved']))
	{
		$event_info = $_SESSION['saved_form_data'];
		
		//unset all variables?
		unset($_SESSION['form_data_saved']);
	}
	
	//data was collected from the form, use it to fill in spots
	
	if (isset($_SESSION['selected_event_id']) && !isset($_POST['selected_event_id']))
	{
		$_POST['selected_event_id'] = $_SESSION['selected_event_id'];
	}
	
	if (isset($_POST['selected_event_id']))
	{	
		
		global $event_info;
		
		$statement = "SELECT * FROM events WHERE event_id=" . $_POST['selected_event_id'];
		
		$result = queryWithAssocResult($statement);
		$event_info = $result[0];
		
		$_SESSION['selected_event_id'] = $_POST['selected_event_id'];
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div class="clear"></div>
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner" style="text-align:center;">
				
					<?php
					//if qualified, make a list of all past and current events, make a form for creating a new event
					if (checkIfAdmin())
					{
					?>
						<?php
							//display all error and success messages
							if (isset($_SESSION['any_errors']))
							{
								if ($_SESSION['any_errors'])
								{
									echo "<div class='errors'>";
									//div errors
									if (isset($_SESSION['new_event_errors']))
										echo $_SESSION['new_event_errors'];
									if (isset($_SESSION['update_event_errors']))
										echo $_SESSION['update_event_errors'];
									if (isset($_SESSION['remove_event_errors']))
										echo $_SESSION['remove_event_errors'];
									echo "</div>";
								}
								else
								{
									echo "<div class='success'>";
									//div success
									if (isset($_SESSION['new_event_message']))
										echo $_SESSION['new_event_message'];
									if (isset($_SESSION['update_event_message']))
										echo $_SESSION['update_event_message'];
									if (isset($_SESSION['remove_event_message']))
										echo $_SESSION['remove_event_message'];
									echo "</div>";
								}
								
								unset($_SESSION['remove_event_message']);
								unset($_SESSION['update_event_message']);
								unset($_SESSION['new_event_message']);
								
								unset($_SESSION['remove_event_errors']);
								unset($_SESSION['update_event_errors']);
								unset($_SESSION['new_event_errors']);
								
								unset($_SESSION['any_errors']);
							}
						?>
						<h2>Event Management</h2>
						<form name="select_event" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
							<select name="selected_event_id">
								<?php
									$link = openDatabase();
									$query = $link->prepare("SELECT event_id, event_name, organization, start_date FROM events WHERE completed=0 ORDER BY start_date");
									if ($query)
									{
										$query->bind_result($temp_event_id, $temp_event_name, $temp_organization, $temp_start_date);
										$query->execute();
										
										while ($query->fetch())
										{
											$selected = '';
											if (isset($_SESSION['selected_event_id']))
											{
												if ($temp_event_id == $_SESSION['selected_event_id'])
												{
													$selected = 'selected';
												}
											}
										?>
											<option value="<?php echo $temp_event_id; ?>" <?php echo $selected; ?> > <?php echo $temp_organization . ": " . $temp_event_name . " (" . dateToString($temp_start_date) . ")"; ?> </option>
										<?php
										}
									}
									else
										addError("Error in the query: 1");
								?>
							</select>
							<br />
							<!-- <input type="submit" value="Complete Event" onclick="select_event.action='phpscripts/complete_event.php'; return true;" /> -->
							<input type="submit" value="Select Event" />
							
							<script>
								function validate()
								{
									select_event.action='phpscripts/remove_event.php';
									var r = confirm('Do you really want to remove this event?');
									if (r == true)
									{
										select_event.submit();
									}
								}
							</script>
							<input type="button" value="Remove Event" onclick="validate();" />
						</form>
					
						<h3>Event Information</h3>
						<form name="event_form" method="post" action="">
							<table align="center">
								<tr>
									<td> Event Name: </td>
									<td> <input name="event_name" type="text" value="<?php echo $event_info['event_name']; ?>" /> </td>
								</tr>
								<tr>
									<td> Organization: </td>
									<td> <input name="organization" type="text" value="<?php echo $event_info['organization']; ?>" /> </td>
								</tr>
								<tr>
									<td> Address: </td>
									<td> <input name="address" type="text" value="<?php echo $event_info['address']; ?>" /> </td>
								</tr>
								<!--
								<tr>
									<td> Map Link (short URL): </td>
									<td> <input name="map_link" type="text" value="<?php //echo $event_info['map_link']; ?>" /> </td>
								</tr>
								-->
								<tr>
									<td> Contact Name: </td>
									<td> <input name="contact_name" type="text" value="<?php echo $event_info['contact_name']; ?>" /> </td>
								</tr>
								<tr>
									<td> Contact Phone: </td>
									<td> <input name="contact_phone" type="text" value="<?php echo $event_info['contact_phone']; ?>" /> </td>
								</tr>
								<tr>
									<td> Contact Fax: </td>
									<td> <input name="contact_fax" type="text" value="<?php echo $event_info['contact_fax']; ?>" /> </td>
								</tr>
								<tr>
									<td> Contact Email: </td>
									<td> <input name="contact_email" type="text" value="<?php echo $event_info['contact_email']; ?>" /> </td>
								</tr>
								<tr>
									<td> Start Date (yyyy-mm-dd): </td>
									<td> <input name="start_date" type="text" value="<?php echo $event_info['start_date']; ?>" /> </td>
								</tr>
								<tr>
									<td> Map Link: </td>
									<td> <input name="map_link" type="text" value="<?php echo $event_info['map_link']; ?>" /> </td>
								</tr>
								<tr>
									<td> Comments: </td>
									<td> <textarea name="comments" rows="4" cols="30"><?php echo $event_info['comments']; ?></textarea> </td>
								</tr>
								<tr>
									<td align="center"> <input type="submit" value="Register Event" onclick="event_form.action='phpscripts/new_event.php'; return true;" /> </td>
									<td align="center"> <input type="submit" value="Update Event" onclick="event_form.action='phpscripts/update_event.php'; return true;" /> </td>
								</tr>
							</table>
							<input type="hidden" name="event_id" value="<?php echo $event_info['event_id']; ?>" />
						</form>
						
						<!--
						*********************************************************
						-->
						
						
						<?php if ($event_info['event_id'] != 0)
						{
						?>
							<div id="new_file">
								<h2>Event File Management</h2>
								<form method="post" action="phpscripts/new_event_file.php" enctype="multipart/form-data">
									<input type="hidden" name="selected_event_id" value="<?php echo $event_info['event_id']; ?>" />
									<table align="center">
										<tr>
											<td> Title: </td>
											<td> <input type="text" name="title" maxlength="100" /> </td>
										</tr>
										<tr>
											<td> Comment: </td>
											<td> <textarea type="text" name="comment" cols="17" rows="4" maxlength="200"></textarea> </td>
										</tr>
										<tr>
											<td> <label for="file">Filename:</label> </td>
											<td> <input type="file" name="file" id="file" /> </td>
										</tr>
										<tr>
											<td colspan="2" style="text-align:center;">
												<input type="submit" value="Create Link" />
											</td>
										</tr>
									</table>
								</form>
							</div>
							
							<table align="center" width="85%">
					
								<tr>
									<td>X</td>
									<td><h3>Title<h3></td>
									<td><h3>Comment<h3></td>
									<td></td>
								</tr>
							
							<?php
								$statement = "SELECT * FROM event_files WHERE event_id=" . $event_info['event_id'] . " ORDER BY title";
								
								$rows = queryWithAssocResult($statement);
									
								foreach ($rows as $row)
								{
								?>
								<tr>
									<?php
										if (checkIfAdmin())
										{
											echo '<td width="5%"><form method="post" action="phpscripts/remove_event_file.php">
												<input type="submit" value="X" />
												<input type="hidden" name="file_id" value="' . $row['file_id'] . '" />
											</form></td>';
										}
									?>
									<td width="35%"> <?php echo $row['title']; ?> </td>
									<td width="45%"> <?php echo $row['comment']; ?> </td>
									<td width="10%">
								<?php
									if (file_exists("phpscripts/files/" . $row['filename']))
									{
								?>
											<a class="button_a" href="<?php echo "phpscripts/download_file.php?filename=" . $row['filename']; ?>">Download</a>
								<?php
									}
								?>
									</td>
								</tr>
							<?php
								}
							?>
								
							</table>
						
						
						<!--
						*********************************************************
						-->
						
							<div id="new_event">
								<!-- SHIFT MANAGEMENT -->
								<h2>Shift Management</h2>
								<!-- form to add a new shift -->
								<form name="new_shift" method="post" action="phpscripts/new_shift.php">
									<input name="event_id" type="hidden" value="<?php echo $event_info['event_id']; ?>" />
									<table align="center" style="text-align:center;">
										<tr>
											<td colspan="2"> <h3> Add Shift to: <?php echo $event_info['event_name']; ?> </h3> </td>
										</tr>
										<tr>
											<td colspan="2"> Date (yyyy-mm-dd) <br /> <input name="date" type="text" /> </td>
										</tr>
										<tr>
											<td> Start Time <br /> <input name="start_time" type="text" maxlength="5" size="5" /> </td>
											<td> End Time <br /> <input name="end_time" type="text" maxlength="5" size="5" /> </td>
										</tr>
										<tr>
											<td>Members Required:</td>
											<td> <input name="required_amount" type="text" maxlength="2" size="2" /> </td>
										</tr>
										<tr>
											<td colspan="2"> <input type="submit" value="Add Shift" /> </td>
										</tr>
									</table>
								</form>
							</div>
							
				
						<?php
							//get all shifts for the selected event
							//query for all members of each shift and put each shift into a select tag
							
							//delete shift
							//update shift
							//remove member
							$get_shifts_statement = "SELECT shift_id, date, start_time, end_time, required_amount FROM shifts WHERE event_id=" . $event_info['event_id'] . " ORDER BY date ASC";
							
								
								$shift_info = queryWithAssocResult($get_shifts_statement);
								
								foreach ($shift_info as $shift)
								{
									?>
									<div class="shift">
										
										<table>
											<!-- Update and remove shift -->
											<form name="existing_shift_form" method="post" action="phpscripts/update_shift.php" style="margin: 0px; padding: 0px; display:inline;">
											
												<input name="selected_event_id" type="hidden" value="<?php echo $event_info['event_id']; ?>" />
												<input type="hidden" name="selected_shift_id" value="<?php echo $shift['shift_id']; ?>" />
												<tr>
													<td colspan="2"> Date (yyyy-mm-dd) <br /> <input name="date" type="text" value="<?php echo $shift['date']; ?>" /> </td>
												</tr>
												<tr>
													<td> Start Time <br /> <input name="start_time" type="text" maxlength="5" size="5" value="<?php echo $shift['start_time']; ?>" /> </td>
													<td> End Time <br /> <input name="end_time" type="text" maxlength="5" size="5" value="<?php echo $shift['end_time']; ?>" /> </td>
												</tr>
												<tr>
													<td>Members Required:</td>
													<td> <input name="required_amount" type="text" maxlength="2" size="2" value="<?php echo $shift['required_amount']; ?>" /> </td>
												</tr>
												<tr>
													<td colspan="2"> <input name="update_shift" type="submit" value="Update Shift" align="center" /> </td>
												</tr>
											</form>
										</table>
										<table>
												<tr>
													<td colspan="2">
														<form method="post" action="phpscripts/remove_shift.php" style="margin: 0px; padding: 0px; display:inline;">
															<input type="hidden" name="selected_shift_id" value="<?php echo $shift['shift_id']; ?>" />
															<input name="remove_shift" type="submit" value="Remove Shift" align="center" />
														</form>
													</td>
												</tr>
											
												<!-- All different users drop-down -->
												<form method="post" action="phpscripts/remove_sign_up.php"  style="margin: 0px; padding: 0px; display:inline;">
													<input name="selected_event_id" type="hidden" value="<?php echo $event_info['event_id']; ?>" />
													<input type="hidden" name="selected_shift_id" value="<?php echo $shift['shift_id']; ?>" />
													<tr>
														<td>
															<select name="user_id">
																<?php
																$get_users_statement = "SELECT user_id FROM sign_ups WHERE shift_id=" . $shift['shift_id'];
																$query_user_ids = $link->prepare($get_users_statement);
																if ($query_user_ids)
																{
																	$query_user_ids->bind_result($user_id);
																	$query_user_ids->execute();
																	$ids = array();
																	while ($query_user_ids->fetch())
																	{
																		$ids[] = $user_id;
																	}
																	
																	$query_user_ids->close();
																}
																	
																$get_info_statement = "SELECT first, last, email FROM members WHERE user_id IN (" . implode(',', array_map('intval', $ids)) . ")";
																$query_get_info = $link->prepare($get_info_statement);
																if ($query_get_info)
																{
																	$query_get_info->bind_result($first, $last, $email);
																	$query_get_info->execute();
																	while ($query_get_info->fetch())
																	{
																	?>
																		<option value="<?php echo $user_id; ?>"><?php echo $first . " " . $last; ?></option>
																	<?php
																	}
																	$query_get_info->close();
																}
																?>
															</select>
														</td>
														<td> <input type="submit" value="Remove Member" /> </td>
													</tr>
												</form>
											</table>
										</form>
									</div>
										
									<?php
								}
										
						} //end of event_id check
					} //end of checkIfAdmin()
					?>
				
				</div> </div>
				<!-- END OF "body_sect" -->
				
			</div>
		</div> <!-- END OF MAIN DIV -->
		
	</body>
</html>