<!-- php session_start() is already in the file that includes this one -->

<div class="paneled centered gradient1" id="control_panel">
	<div class="white_bold major_title"> Hello, <?php echo $_SESSION['first']; ?> </div>
	<div>
		<br />
		<?php 
		if ($_SESSION['position'] === 'Coordinator' or $_SESSION['position'] === 'Admin') //this is only if you are an admin
		{
		?>
			<?php
			if ($_SESSION['last'] === 'Fehr' or $_SESSION['position'] === 'Admin')
			{
			?>
				<a href='member_activity_log.php'>Activity</a><br />
			<?php	
			}
			?>
			<a class="button_a" href="member_management_page.php">Members</a>
			<br />
			<a class="button_a" href="event_management_page.php">Event Management</a>
			<br />
		<?php
		}
		?>
		
		<a class="button_a" href="change_info_page.php">Manage Account</a> <br />
		<a class="button_a" href="my_events_page.php">My Registered Events</a> <br />
		<a class="button_a" href="all_events_page.php">Event Sign Up</a> <br />
		<a class="button_a" href="meeting_management_page.php">Meetings</a> <br />
		<a class="button_a" href="training_management_page.php">Training</a> <br />
		<a class="button_a" href="member_contact_info.php">Member Contact</a> <br />
	</div>
	
	<form method="post" action="phpscripts/logout.php">
		<input type="submit" value="Log Out" />
	</form>
</div>