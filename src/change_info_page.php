<?php
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div class="clear"></div>
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner">
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT COLOR="#ff0000"><FONT SIZE=6 STYLE="font-size: 22pt">Canadian
					Red Cross </FONT></FONT>
					</P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT SIZE=5>Windsor/
					Essex Branch</FONT></P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><A NAME="_GoBack"></A><FONT SIZE=4 STYLE="font-size: 16pt">3909
					Grand Marais Rd E.</FONT></P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT SIZE=4 STYLE="font-size: 16pt">Windsor,
					ON</FONT></P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT SIZE=4 STYLE="font-size: 16pt">N8W
					1W9</FONT></P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT SIZE=4 STYLE="font-size: 16pt">T:
					  519-944-8144		F:   519-974-1424</FONT></P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><BR><BR>
					</P>
					<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><BR><BR>
					</P>
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
				
			</div>
		</div> <!-- END OF MAIN DIV -->
		
	</body>
</html>