<?php
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="main_styles.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		
		<!-- THIS IS THE START OF "top_sect" -->
			<script src="includes/top_sect.js" type="text/javascript"></script>
		<!-- THIS IS THE END OF "top_sect" -->
		
		<div id="main_sect">
			<div style="width:100%;">
				<div id="left_sect">
					
					<?php include('includes/login_manager.php'); ?>
					
					<br />
					
					<script src="includes/navigation.js" type="text/javascript"></script>
				</div>
				
				<!-- THIS IS THE START OF "body_sect" -->
				<div id="body_sect"> <div id="body_sect_inner">
				
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%">
					<FONT FACE="Verdana, serif"><FONT SIZE=1 STYLE="font-size: 7pt"><FONT SIZE=2>The
					Canadian Red Cross, Windsor Essex County Branch, First Aid Services
					Team (FAST) is a group of qualified volunteers available to provide
					First Aid Services at community events. </FONT></FONT></FONT>
					</P>
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%">
					<FONT FACE="Verdana, serif"><FONT SIZE=1 STYLE="font-size: 7pt"><FONT SIZE=2>It
					would be ideal if everyone knew what to do when suddenly confronted
					with an emergency, but that is not the reality. Instead, people tend
					to look to others who are more knowledgeable about what care to
					provide to an injured or ill person. First Aid service will be
					provided as outlined in the Canadian Red Cross Standard First Aid and
					First Responder Certification programs. </FONT></FONT></FONT>
					</P>
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%"><A NAME="_GoBack"></A>
					<FONT FACE="Verdana, serif"><FONT SIZE=1 STYLE="font-size: 7pt"><FONT SIZE=2>FASTeam
					members are trained in the use of </FONT><FONT SIZE=2><B>Cardio
					Pulmonary Resuscitation- Health Care Provider Level</B></FONT><FONT SIZE=2>,
					Automated External Defibrillation, Oxygen Administration, spinal
					immobilization techniques and the latest in First Aid Training.</FONT></FONT></FONT></P>
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%">
					<FONT FACE="Verdana, serif"><FONT SIZE=1 STYLE="font-size: 7pt"><FONT SIZE=2>Having
					prompt and immediate First Aid services on-site can make the
					difference between an injury and permanent disability. Secure your
					event and contract on First Aid Services from Canadian Red Cross
					Windsor Branch FASTeam!</FONT></FONT></FONT></P>
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%">
					<BR><BR>
					</P>
					<P STYLE="margin-top: 0.02in; margin-bottom: 0.02in; line-height: 100%">
					<FONT FACE="Verdana, serif"><FONT SIZE=1 STYLE="font-size: 7pt"><FONT SIZE=1>All
					supplies, transportation and other associated costs are all inclusive
					of the FASTeam. FASTeam reserves the right to change any specifics
					of/about the signed contract at any time with no notice required.
					FASTeam carries independent insurance provided by Canadian Red Cross.
					Each of FASTeam volunteers must provide proof of SFA CPR- HCP, two
					forms of ID, Clear Police Record with clear Vulnerable Sector Check
					and must remain free and clear of all criminal offences to maintain a
					membership aboard FASTeam.</FONT></FONT></FONT></P>
					
				</div> </div>
				<!-- THIS IS THE END OF "body_sect" -->
			</div>
		</div> <!-- END OF MAIN DIV -->
	</body>
</html>