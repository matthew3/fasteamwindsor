# README #

### What is this repository for? ###

This project is a website voluntarily constructed for FASTeam Windsor, which was the first aid services team for various Windsor and Essex county events. This website was developed to support the volunteer management needs of the team. It allows management to create events that describe the event, show the location on the map, and consist of various time slots for which volunteers are required. Volunteers can voluntarily apply to event time slots, or they can be assigned to event time slots by management.